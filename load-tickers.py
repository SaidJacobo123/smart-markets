import yfinance as yf
import pandas as pd
from sklearn.model_selection import train_test_split


def get_data_and_save_min(tickers, min_size):

    for ticker in tickers:
        df = yf.download(ticker)
        
        #df.reset_index(inplace=True,drop=False)
        df.rename(columns={'Open':'open',
                            'High':'high',
                            'Low':'low',
                            'Close':'close',
                            'Adj Close':'adj_close',
                            'Volume':'volume'}, inplace=True)

        df_min, _ = train_test_split(df, train_size=float(min_size), shuffle=False)

        df.to_csv('/home/said/Documentos/Proyectos/smart-markets/data/{}.csv'.format(ticker))
        df_min.to_csv('/home/said/Documentos/Proyectos/smart-markets/daily/{}_min.csv'.format(ticker))

        print()

if __name__ == "__main__":

    while True:
        tikers = input('Ingrese los tickers separados por coma: ')
        tikers = tikers.split(',')

        min_size = input('Ingrese el tamaño del set de prueba: ')

        get_data_and_save_min(tikers, min_size)