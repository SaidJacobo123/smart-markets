import talib
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
import pytz
import numpy as np

class Utils:
    def get_all_indicators(self, dataframe):
        candles_indicators = dataframe.copy()    
        candles_indicators["rsi"] = talib.RSI(candles_indicators["close"], timeperiod=14)
        candles_indicators["ema_12"] = talib.EMA(candles_indicators["close"], timeperiod=12)
        candles_indicators["ema_26"] = talib.EMA(candles_indicators["close"], timeperiod=26) 

        upper_band, middle_band, lower_band = talib.BBANDS(candles_indicators["close"], timeperiod=20, 
                                                    nbdevup=2, nbdevdn=2, matype=0)

        candles_indicators["upper_bband"] = upper_band
        candles_indicators["middle_bband"] = middle_band
        candles_indicators["lower_bband"] = lower_band


        macd, macd_signal, macd_hist = talib.MACD(candles_indicators["close"], fastperiod=12, slowperiod=26, signalperiod=9)

        candles_indicators["macd"] = macd 
        candles_indicators["macd_signal"] = macd_signal 
        candles_indicators["macd_hist"] = macd_hist
            
        return candles_indicators
    
    def make_feature_engineering(self, df, drop_cols):

        df['percentage_price_change'] = ((df['close'] - df['close'].shift(1))/df['close'].shift(1))*100
        
        #Date
        df.index = df.index.tz_localize('US/Eastern').tz_convert(pytz.UTC)
        df["date"] = pd.to_datetime(df.index)
        df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d %H:%M:%S')

        df["hour"] = df["date"].dt.hour
        df["day"] = df["date"].dt.day
        df["day_of_week"] = df["date"].dt.weekday
        df["week"] = df["date"].dt.week
        df["month"] = df["date"].dt.month
        df["quarter"] = df["date"].dt.quarter
        df["year"] = df["date"].dt.year
        
        #RSI
        df['discrete_rsi'] = 0
        df.loc[df.rsi >= 70, 'discrete_rsi'] = 1
        df.loc[df.rsi <= 30, 'discrete_rsi'] = -1
        
        #EMA
        df['discrete_ema12_ema26'] = -1
        df.loc[df.ema_12 > df.ema_26, 'discrete_ema12_ema26'] = 1
        
        df['discrete_ema_12'] = 0
        df.loc[df.close > df.ema_12, 'discrete_ema_12'] = 1
        df.loc[df.close < df.ema_12, 'discrete_ema_12'] = -1

        df['discrete_ema_26'] = 0
        df.loc[df.close > df.ema_26, 'discrete_ema_26'] = 1
        df.loc[df.close < df.ema_26, 'discrete_ema_26'] = -1
        
        #Bolinger bands
        df['discrete_bollinger_bands'] = 0
        df.loc[df.close > df.upper_bband, 'discrete_bollinger_bands'] = 1
        df.loc[df.close < df.lower_bband, 'discrete_bollinger_bands'] = -1
        
        #MACD
        df['discrete_macd_signal'] = 0
        df.loc[df.macd_signal > 0, 'discrete_macd_signal'] = 1

        df['discrete_macd_hist'] = 0
        df.loc[df.macd_hist > 0, 'discrete_macd_hist'] = 1

        df['discrete_macd_hist_signal'] = 0
        df.loc[df.macd_hist > df.macd_signal, 'discrete_macd_hist_signal'] = 1
        
        #Candles Features
        df['distance_close_ema_12'] = np.absolute(df['close'] - df['ema_12'])
        df['distance_close_ema_26'] = np.absolute(df['close'] - df['ema_26'])
        df['distance_close_upper_bband'] = np.absolute(df['close'] - df['upper_bband'])
        df['distance_close_middle_bband'] = np.absolute(df['close'] - df['middle_bband'])
        df['distance_close_lower_bband'] = np.absolute(df['close'] - df['lower_bband'])

        df['distance_high_low'] = np.absolute(df['high'] - df['low'])
        df['distance_open_close'] = np.absolute(df['open'] - df['close'])
        df['distance_high_open'] = np.absolute(df['high'] - df['open'])
        df['distance_high_close'] = np.absolute(df['high'] - df['close'])
        df['distance_open_low'] = np.absolute(df['open'] - df['low'])
        df['distance_close_low'] = np.absolute(df['close'] - df['low'])

        df['bullish_candle'] = 0
        df.loc[df.close > df.open, 'bullish_candle'] = 1

        #Shift
        columns_to_shift = ['percentage_price_change',
                            'discrete_rsi',
                            'discrete_ema12_ema26',
                            'discrete_ema_12',
                            'discrete_ema_26',
                            'discrete_bollinger_bands',
                            'discrete_macd_signal',
                            'discrete_macd_hist',
                            'discrete_macd_hist_signal',
                            'distance_close_ema_12',
                            'distance_close_ema_26',
                            'distance_close_upper_bband',
                            'distance_close_middle_bband',
                            'distance_close_lower_bband',
                            'distance_high_low',
                            'distance_open_close',
                            'distance_high_open',
                            'distance_high_close',
                            'distance_open_low',
                            'distance_close_low',
                            'bullish_candle']
        amount_shift = 15

        for column in columns_to_shift:
            for x in range(1, amount_shift + 1):
                new_column = column + '_shifted_' + str(x)
                df[new_column] = df[column].shift(x)

        df['eur_usd'] = 0
        df['usd_jpy'] = 0
        df['gbp_usd'] = 0

        df.drop(columns=drop_cols, inplace=True)
        
        return df

    def create_target(self, dataframe, periods_fordward):
        dataframe['difference'] = -dataframe['close'].diff(-periods_fordward)
        
        dataframe.loc[dataframe.difference > 0, 'target'] = 1
        dataframe.loc[dataframe.difference < 0, 'target'] = 0
        
        dataframe.drop(columns=['difference','open','high','low','close'], inplace=True)
        
        return dataframe
    
    def get_model(self, path=None):

        if path == None:
            model = Sequential()
            model.add(Dense(256, input_dim=59, activation="relu"))
            model.add(Dropout(0.2))

            model.add(Dense(64,activation="relu"))
            model.add(Dropout(0.2))

            model.add(Dense(16,activation="relu"))
            model.add(Dropout(0.2))

            model.add(Dense(1,activation = "sigmoid")) 

            model.compile(optimizer = "Adam",loss="binary_crossentropy",metrics=["accuracy"])
        else:
            model = load_model(path)

        return model

    def get_data_from_csv(self, path):
        data = pd.read_csv(path)
        return data

