import time
from alpha_vantage.timeseries import TimeSeries
import os
from utils import Utils
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
import numpy as np
import pandas as pd
from datetime import datetime
from ensemble import Ensemble
import telebot
from apscheduler.schedulers.blocking import BlockingScheduler

def send_message(message, token, chat_id):
    try:
        tb = telebot.TeleBot(token) 
        tb.send_message(chat_id, message)
    except (ConnectionAbortedError, ConnectionResetError, ConnectionRefusedError, ConnectionError):
        print("ConnectionError - Sending again after 5 seconds!!!")
        time.sleep(5)
        tb = telebot.TeleBot(token) 
        tb.send_message(chat_id, message)


def work():
    tickers = ['EURUSD','USDJPY','GBPUSD']
    utils = Utils()
    api_key = os.environ['ALPHAVANTAGEKEY']
    chatid = os.environ['CHATID'] 
    ensemble = Ensemble()
    token = os.environ['TOKENBOT'] # Ponemos nuestro Token generado con el @BotFather
    ts = TimeSeries(key=api_key, output_format='pandas')
    #model = utils.get_model('./models/model_16_ephocs_new_features.h5')

    #while True:

    for ticker in tickers:
        print('='*32)
        print('Pidiendo datos de ', ticker)

        df, meta_data = ts.get_intraday(symbol=ticker, interval='60min', outputsize='full')
        
        print('last refreshed: ', meta_data['3. Last Refreshed'])
        
        print('Realizando preprocesado...')

        df.rename(columns={'1. open':'open',
                            '2. high':'high',
                            '3. low':'low',
                            '4. close':'close', 
                            '5. volume':'volume'}, inplace=True)
        
        actual_price = df.iloc[-1].close
        
        df = utils.get_all_indicators(df)
        
        drop_cols= ['date','rsi', 'ema_12', 'ema_26', 'upper_bband', 'middle_bband', 'lower_bband', 'macd', 'macd_signal', 'macd_hist', 'volume']
        df = utils.make_feature_engineering(df, drop_cols)
        
        df = utils.create_target(df, 5)
        
        scaler = MinMaxScaler(feature_range=(0, 1))
        scaled_features = scaler.fit_transform(df)
        df = pd.DataFrame(scaled_features, index=df.index, columns=df.columns)

        df['eur_usd'] = 1 if ticker == 'EURUSD' else 0
        df['usd_jpy'] = 1 if ticker == 'USDJPY' else 0
        df['gbp_usd'] = 1 if ticker == 'USDGBP' else 0

        x_val = df.tail(1).drop(columns='target') #la creo aca porque se eliminara luego del .dropna()

        df.dropna(inplace=True)
    

        #------------------------------------Prediccion para operar----------------------------

        val_predict, confidence, list_preds = ensemble.predict(x_val)
        print('Enviando mensaje...')

        text = ''
        take_profit=0
        stop_loss = 0
        action = ''
        
        if val_predict >= 0.5:
            #confidence = val_predict[0][0]
            action = 'COMPRAR'
            take_profit = round(actual_price + 0.0015, 4) if ticker != 'USDJPY' else round(actual_price + 0.015, 4)
            stop_loss = round(actual_price - 0.0005, 4) if ticker != 'USDJPY'else round(actual_price - 0.005, 4)

        else:
            #confidence = 1 - val_predict[0][0]
            action = 'VENDER'
            take_profit = round(actual_price - 0.0015, 4) if ticker != 'USDJPY' else round(actual_price - 0.015, 4)
            stop_loss = round(actual_price + 0.0005, 4) if ticker != 'USDJPY' else round(actual_price + 0.005, 4)

        date = datetime.now()
        date = date.strftime('%d/%m/%Y %H:%M:%S')

        confidence = np.round(confidence, 3)
        text = '---{}--- \n {}: {}\n Precio actual: {} \n Stoploss: {} \n TakeProfit: {}'.format(date, 
                                                                                                action, 
                                                                                                ticker, 
                                                                                                actual_price, 
                                                                                                stop_loss, 
                                                                                                take_profit)
        text += ' \n Confianza: {} \n total de predicciones: {}'.format(confidence, list_preds)
        '''            
        text += ' \n Train accuracy: {} \n Test accuracy {} \n Confianza: {}'.format(train_accuracy,
                                                                                    test_accuracy,
                                                                                    confidence)'''
        
        if confidence > 0.8:
            send_message(text, token, chatid)


        print(text)
        print('='*32)

        time.sleep(5)

    #time.sleep(3600)

if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(work, 'cron', minute=0)
    scheduler.start()


#work()

