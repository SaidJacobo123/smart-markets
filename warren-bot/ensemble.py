from joblib import load
from keras.models import load_model
import numpy as np
import statistics as stats

class Ensemble:


    def __init__(self):
        self.ada_boost = load('./models/ab_10_estimators.joblib')
        self.decition_tree = load('./models/dt_max_depth_8.joblib')
        self.gradient_boosting = load('./models/gb_100_estimators.joblib')
        self.random_forest = load('./models/rf_50_estimators_3_max_depth_3.joblib')
        self.neural_network = load_model('./models/model_32_ephocs_new_features.h5')

        self.models = [self.random_forest, self.gradient_boosting, self.decition_tree, self.ada_boost, self.neural_network]
        
        self.ponderations = {
            self.random_forest : 1, 
            self.gradient_boosting : 2, 
            self.decition_tree : 2,
            self.ada_boost : 1, 
            self.neural_network : 3
            }        


    def fit(self, x, y):
        pass

    def predict(self,x):
        preds = []
        count = 0

        nn_pred = None

        for m in self.models:
            ponderation = self.ponderations[m] + 1 # <-- Por el range
            count += 1

            prediction = m.predict(x)

            if m == self.neural_network:
                nn_pred = prediction

            for i in range(1, ponderation):
                preds.append(int(prediction))

        final_pred = stats.mode(preds) # <-- Voto por mayoria
        confidence = preds.count(final_pred) / len(preds)
        
        preds.append(round(nn_pred[0][0], 3))

        return final_pred, confidence, preds