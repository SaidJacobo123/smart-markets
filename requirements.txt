zipline==1.4.0
ipykernel==5.3.4
scikit-learn==0.23.1
tensorflow==2.3.0
TA-lib==0.4.18
matplotlib==3.3.0
keras==2.4.3
